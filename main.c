#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "oled.h"

/* 毫秒级 延时 */
void sleep_ms(int ms)
{
	struct timeval delay;
	delay.tv_sec = 0;
	delay.tv_usec = ms * 1000;
	select(0, NULL, NULL, NULL, &delay);
}

int main(void)
{
    int len = 0;
    struct tm *p;
    time_t *timep = malloc(sizeof(*timep));
    char *day_t = malloc(32);
    char *time_t = malloc(32);

    oled_init();

    oled_P8x16Str(0, 0, "STM32MP157");

    while(1)
    {
        time(timep);
        p = gmtime(timep);
        sprintf(day_t, "%d/%d/%d",(1900 + p->tm_year),(1 + p->tm_mon),p->tm_mday);
        sprintf(time_t, "%d:%d:%02d",p->tm_hour+8,p->tm_min,p->tm_sec);

        //printf("%s %s\n",day_t, time_t);

        oled_P8x16Str(0, 2, day_t);
        oled_P8x16Str(0, 4, time_t);
        sleep_ms(100);
    }

    close(fd);

    return 0;
}

